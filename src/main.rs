extern crate sha2;
extern crate rand;
extern crate hex_slice;

use sha2::{Sha256, Digest};
use rand::{thread_rng, Rng};
use std::thread;
use hex_slice::AsHex;


fn main() {
    let mut children = vec![];

    for _i in 0..4 {
        children.push(thread::spawn(|| {
            generate_hash();
        }));
    }

    for child in children {
        let _ = child.join();
    }

    // handle.join().unwrap();
}


fn generate_hash() {
    let mut zeros: u32;
    let mut input = [0u8; 32];
    
    loop {
        let mut hasher = Sha256::default();
        thread_rng().fill(&mut input[..]);
        hasher.input(&input);
        let output = hasher.result();

        zeros = 0;

        for element in output.iter() {
            // println!("{}", *element);
            if *element == 0 {
                zeros += 2;
            
            } else if *element < 16 {
                zeros += 1;
                break;

            } else {
                break;
            }
        }

        if zeros > 5 {
            println!("Hashen  {:x} hadde {} nullar", output, zeros);
            println!("Laga av {:02x}\n", input.plain_hex(false));
        }
    }
}

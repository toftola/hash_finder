# Hash Finder

Finds hashes with as many leading zeros as possible. Written in Rust.

## Install Rust on Windows

### Install dependencies
1. Download and install [Visual Studio Community 2017](https://visualstudio.microsoft.com/thank-you-downloading-visual-studio/?sku=Community&rel=15)
2. From the Visual Studio Installer, choose "Individual components" and install:
   - Windows Universial CRT SDK
   - Windows 8.1 SDK
   - VC++ 2015.3 v14.00 (v140) toolset for desktop

### Install Rust itself
Download the [Rust installer](https://win.rustup.rs/), run it and follow the instructions on screen.

## Clone this repository
If you don't have Git installed, [download](https://git-scm.com/downloads) and install Git.  
From your command line, navigate to where you want to clone the repository and run
```
$ git clone https://gitlab.com/toftola/hash_finder.git
$ cd hash_finder
```

## Run
To compile and run optimized, execute
```
$ cargo build --release
$ cargo run --release
```
This will take some more time to compile the first time, since Rust needs to download and compile the dependencies.

While developing, you can also run
```
$ cargo run
```
This will both compile and run the program unoptimized. It compiles faster, but runs slower.

## Things to change
The most important file is src/main.rs. Some interesting lines to edit:

- Line 12
   ```
   for _i in 0..4 {
   ```
   This spesifies the number of cores. If, for instance, your machine has 8 cores, and you want to use them all, you can change it to
   ```
   for _i in 0..8 {
   ```

- Line 52
   ```
   if zeros > 5 {
   ```
   This says that all hashes with more leading zeros than 5 will be printed. Change it to what you like.
